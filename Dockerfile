FROM memgraph:0.14.0

USER root

RUN mkdir -p /opt/project  /data
WORKDIR /opt/project
 
RUN apt-get update
RUN apt-get install -y --no-install-recommends wget tar python net-tools
RUN python -V
ENV PYTHON_PIP_VERSION=18.1
RUN set -ex; \
	\
	wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'; \
	\
	python get-pip.py \
		--disable-pip-version-check \
		--no-cache-dir \
		"pip==$PYTHON_PIP_VERSION" \
	; \
	pip --version; \
	\
	find /usr/local -depth \
		\( \
			\( -type d -a \( -name test -o -name tests \) \) \
			-o \
			\( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \
		\) -exec rm -rf '{}' +; \
	rm -f get-pip.py

RUN pip install numpy websockify  

COPY ./scripts/memgraph.sh /opt/project
RUN chmod +x /opt/project/memgraph.sh

RUN apt-get install -y grep procps

EXPOSE 7688

USER memgraph

#RUN cat /etc/memgraph/memgraph.conf
#RUN /usr/lib/memgraph/memgraph -help
ENTRYPOINT [ "/opt/project/memgraph.sh" ]
CMD [""]