
### 1. build

```
./scripts/build.sh build
```

### 2. test

```
./scripts/build.sh build \
&&
docker stop memgraph \
&& \
docker rm memgraph \
&& \
docker run -it -d --name memgraph \
-p 8688:7688 \
-p 8687:7687 \
memgraph

```
