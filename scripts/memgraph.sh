#!/bin/bash
## refer https://docs.docker.com/config/containers/multi-service_container/


#1. Start the Memgraph  process
## defaul config path /etc/memgraph/memgraph.conf 
/usr/lib/memgraph/memgraph --flag-file /etc/memgraph/memgraph.conf &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start Memgraph DB: $status"
  exit $status
else
  echo "start Memgraph DB: Success"
fi 

#2. Start the Websockify process
websockify 0.0.0.0:7688 localhost:7687 --cert=/etc/memgraph/ssl/cert.pem --key=/etc/memgraph/ssl/key.pem  --ssl-target --ssl-only -D

status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start Websockify: $status"
  exit $status
else
  echo "start Websockify: Success"
fi 

while sleep 60; do
  ps aux | grep memgraph |grep -q -v grep
  PROCESS_1_STATUS=$?
  ps aux | grep websockify |grep -q -v grep
  PROCESS_2_STATUS=$?
   if [ $PROCESS_1_STATUS -ne 0 -o $PROCESS_2_STATUS -ne 0 ]; then
    echo "Memgraph DB or Websockify has already exited."
    exit 1
  fi
done